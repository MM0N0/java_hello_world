let
    fixed_pkgs = import ./pinned.nix;
in
[

    fixed_pkgs.gnumake
    
    # full version: openjdk 17.0.7+7
    fixed_pkgs.openjdk17-bootstrap

    # full version: gradle 8.4
    fixed_pkgs.gradle_8



]
