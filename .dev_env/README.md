# dev_env
this is a development environment based on direnv and nix-shell.

it was created using the copier template [dev_env_template](https://gitlab.com/MM0N0/dev_env_template).

## template

### install
install latest version:
```bash
copier copy https://gitlab.com/MM0N0/dev_env_template .
```

install a specific version:
```bash
copier copy --vcs-ref v1.0 https://gitlab.com/MM0N0/dev_env_template .
```

### update

update to latest version:
```bash
copier update -a .dev_env/.copier_answers.yml
```

update to a specific version:
```bash
copier update --vcs-ref v1.0.1 -a .dev_env/.copier_answers.yml
```

add `-f` to the command to choose all values from answerfile and don't prompt anymore:
```bash
copier update --vcs-ref v1.0.1 -a .dev_env/.copier_answers.yml -f
```

### excluded from updates
- [project.conf](project.conf)
  (set env vars)
- [nix/packages.nix](nix/packages.nix)
  (define packages)
- [docker/entrypoint.sh](docker/entrypoint.sh)
  (implement project specific logic)

## dependencies
- bash

or for docker dev_env: (for all people not willing to install nix)
- docker

for nix dev_env: (optional, if you want to use the docker image)
- [direnv](https://direnv.net/)
- [nix](https://nixos.org/download/#download-nix)

note: if you are using windows, you will have to use wsl

## usage (nix)
enter the project dir with your terminal and allow direnv to load .envrc with:
```bash
direnv allow
```
A nix-shell with all packages from packages.nix will be loaded.

## usage (docker)
1. enter the project dir with your terminal.
2. the first time you will have to build the dev_env image with:
```bash
.dev_env/docker/build.sh
```

3. push with:
```bash
.dev_env/docker/push.sh
```

4. enter dev_env with:
```bash
./dev_env.sh
```

## ci

### gitlab
add to .gitlab-ci.yml:

```
variables:
  DOCKER_HOST: tcp://docker:2375/
  DOCKER_DRIVER: overlay2
  NO_TTY: 1

image:
  name: docker/compose:latest

services:
  - docker:dind

before_script:
  - apk update
  - apk add --no-cache bash

build_dev_env_image:
  stage: test
  script:
    - echo "$<GITLAB VARIABLE NAME OF DOCKEHUB TOKEN>" | docker login -u <DOCKERHUB USERNAME> --password-stdin
    - ./.dev_env/docker/build.sh
    - ./.dev_env/docker/push.sh
  rules:
    - changes:
      - .dev_env/docker/*
      - .dev_env/nix/pinned.nix
      - .dev_env/nix/base_packages.nix
      - .dev_env/nix/packages.nix
```
